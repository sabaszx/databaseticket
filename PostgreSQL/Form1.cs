﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;  //ใช้ในการเล่นเสียง
using System.IO;

namespace PgSql
{
    
    public partial class Form1 : Form
   {
        public delegate void BarcodeReceivedEventHandler(object sender, BarcodeReceivedEventArgs e); 

        public class BarcodeReceivedEventArgs : EventArgs
        {

            private string _barcode;

            public BarcodeReceivedEventArgs(string Barcode)
            {

                _barcode = Barcode;

            }

            public string Barcode { get { return _barcode; } }

        }

        public event BarcodeReceivedEventHandler BarcodeReceived;

        const char ScannerEndCharacter = '\r';

        private StringBuilder scannedCharacters;

        List<string> dataItems = new List<string>();

      public Form1()
      {
            
            InitializeComponent();
            StartTimer();   //เรียกใช้ฟังก์ชันเวลา
            scannedCharacters = new StringBuilder();

            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(Form1_KeyPress); //เพื่อตรวจสอบการอ่านของเครื่องอ่าน 

            this.BarcodeReceived += new BarcodeReceivedEventHandler(Form1_BarcodeReceived); //หากมีการอ่านของบาร์โค้ดจะเรียกใช้ฟังก์ชันในการอ่าน
        }

     

        private void TbDataItems_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        System.Windows.Forms.Timer tmr = null;
        private void StartTimer() //ฟังก์ชันเวลา
        {
            tmr = new System.Windows.Forms.Timer();
            tmr.Interval = 1000;
            tmr.Tick += new EventHandler(tmr_Tick);
            tmr.Enabled = true;
        }
        void tmr_Tick(object sender, EventArgs e)  //ฟังก์ชันที่ใช้แสดงเวลาแบบ RealTime 
        {
            textBox1.Text = DateTime.Now.ToString();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)//ฟังก์ชันที่ตรวจสอบการอ่านของเครื่องอ่าน
        {
            if (e.KeyChar == 13)    //การแสกนจะตัดที่การ Enter (keychar==13 คือ Enter)
            {
                e.Handled = true;
                OnBarcodeRecieved(new BarcodeReceivedEventArgs(scannedCharacters.ToString()));
                scannedCharacters.Remove(0, scannedCharacters.Length);
            }

            else
            {
                scannedCharacters.Append(e.KeyChar);
                e.Handled = false;
            }
        }

        protected virtual void OnBarcodeRecieved(BarcodeReceivedEventArgs e)//ฟังก์ชันที่ใช้จัการหลังตรวจสอบว่าเครื่องอ่านทำการอ่านได้
        {
            if (BarcodeReceived != null)
                BarcodeReceived(this, e); //ส่งค่ากลับไปที่ Form1 ก่อน Form1 จะเรียกใช้ฟัง์ชัน Form1_BarcodeReceived

        }

        void Form1_BarcodeReceived(object sender, BarcodeReceivedEventArgs e) //ฟังก์ชันอ่านบาร์โค้ด
        {
            //นำค่าที่ได้จากการอ่านบาร์โค้ดไป query ใน database
            //MessageBox.Show("Barcode scanned: " + e.Barcode);
            if (e.Barcode.Length == 10)  //หากอ่านบาร์โค้ดได้ทั้งหมด 10 ตัวอักษร (ถ้าเพิ่มความยาวของบาร์โค้ด ต้องแก้เลข 10)
            {
                String str = null;
                string esString = null;
                string typeString = null;
                string idString = null;
                string keyString = null;
                str = e.Barcode;
                //ทำการ subString เพื่อแยก code ที่อ่านได้เป็นส่วนๆ เช่น id ประเภทของticket เป็นต้น
                esString = str.Substring(0, 2);
                typeString = str.Substring(2, 1);
                idString = str.Substring(3, 4);
                keyString = str.Substring(7, 3);
                //นำค่าที่ได้ไปค้นหาในฐานข้อมูล
                PostGreSQL pgTest = new PostGreSQL();
                dataItems = pgTest.QueryToDay(typeString, idString, keyString);
                tbDataItems.Clear(); //เคลียพื้นที่แสดงผล

                for (int i = 0; i < dataItems.Count; i++) //แสดงผลลัพธ์ที่ได้
                {
                    tbDataItems.Text += dataItems[i];
                    tbDataItems.ScrollToCaret();
                }
                if(dataItems[0] != "")  //หากผลลัพธ์มีในฐานข้อมูลจะทำการเล่นเสียง
                {
                    PlaySound1();   //เล่นเสียงยินดีต้อนรับ
                }
                else  //หากผลลัพธ์ไม่มีในฐานข้อมูล
                {
                    PlaySound2();   //เล่นเสียงไม่พบข้อมูล
                }
            }
            else //หากอ่านบาร์โค้ด ไม่ได้ความยาว 10 จะเล่นเสียงไม่พบข้อมูล
            {
                tbDataItems.Clear();  //เคลียพื้นที่แสดงผล
                PlaySound2();    //เล่นเสียงไม่พบข้อมูล
            }

        }

        private void PlaySound1() //ฟังก์ชันเล่นเสียงยินดีต้อนรับ
        {
            SoundPlayer player1 = new SoundPlayer();
            player1.SoundLocation = @"\databaseticket\ยินดีต้อนรับค่ะ.wav";
            player1.Play();
        }

        private void PlaySound2() //ฟังก์ชันเล่นเสียงไม่พบข้อมูล
        {
            SoundPlayer player2 = new SoundPlayer();
            player2.SoundLocation = @"\databaseticket\ไม่พบข้อมูลค่ะ.wav";
            player2.Play();
        }

    }
}
